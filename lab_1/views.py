from django.shortcuts import render
from datetime import date

# Enter your name here
mhs_name = 'Shafira Fitri' # TODO Implement this
birth_year = 1999

# Create your views here.
def index(request):
    umur = calculate_age(birth_year)
    response = {'name': mhs_name,'age':umur} 
    return render(request, 'index.html', response)

# TODO Implement this to complete last checklist
def calculate_age(birth_year):
    today = date.today()
    return today.year-birth_year
    pass

